var fuente = 0

lamp = [
  {"d":50, "s":0},
  {"d":50, "s":0},
  {"d":50, "s":0},
  {"d":50, "s":0}
]

function set_dimmer(value, change, dimmer, checkLamp){
  var valueTemp =  (lamp[value].d+change <= 100 && lamp[value].d+change >= 0)? lamp[value].d+ change : lamp[value].d
  if (lamp[value].d != valueTemp){
    lamp[value].d = valueTemp
    console.log(lamp[value].d)
    document.getElementById(dimmer).style = `width:${lamp[value].d}%;`
    document.getElementById(dimmer).innerText = lamp[value].d +"%"
    if(lamp[value].s == 1){
      run(`lamp${value}.dimmer(${lamp[value].d})`)
    }
  }
}

function switchLamp(element){
  console.log(element.id)
  switch (element.id) {
    case 'lamp1':
      if (element.checked) {
        console.log("up")
        lamp[0].s = 1
        run(`lamp0.dimmer(${lamp[0].d})`)
      }else{
        console.log("down")
        lamp[0].s = 0
        run(`lamp0.dimmer(0)`)
      }
      break;
    case 'lamp2':
      if (element.checked) {
        console.log("up")
        lamp[1].s = 1
        run(`lamp1.dimmer(${lamp[1].d})`)
      }else{
        console.log("down")
        lamp[1].s = 0
        run(`lamp1.dimmer(0)`)
      }
      break;
    case 'lamp3':
      if (element.checked) {
        console.log("up")
        lamp[2].s = 1
        run(`lamp2.dimmer(${lamp[2].d})`)
      }else{
        console.log("down")
        lamp[2].s = 0
        run(`lamp2.dimmer(0)`)
      }
      break;
    case 'lamp4':
      if (element.checked) {
        console.log("up")
        lamp[3].s = 1
        run(`lamp3.dimmer(${lamp[3].d})`)
      }else{
        console.log("down")
        lamp[3].s = 0
        run(`lamp3.dimmer(0)`)
      }
      break;
    case 'fuente':
      if (element.checked) {
        console.log("up")
        fuente = 1
        run(`fuente.high()`)
      }else{
        console.log("down")
        fuente = 0
        run(`fuente.low()`)
      }
      break;
    default:
      break
  }
}


//Run code python
function runCode(){
  //Blockly.Python.INFINITE_LOOP_TRAP = null;
  var code = 'import pyb\npyb.LED(1).on()\n\x05\n\nfrom pyb import Pin\npyb.LED(1).off()\n';
  //code += Blockly.Python.workspaceToCode(workspace);
  code += '\x04\n';
  enviar(code);
}

function consumo_kw(){
  var pot = 0
  var potFuente = 30
  var maxpot = 76*4 + potFuente
  for ( var i in lamp){
    pot += lamp[i].s*lamp[i].d
  }
  pot = 76*pot/100
  pot += (fuente==1)?potFuente:0
  document.getElementById('label_consumo').innerText = "Potencia requerida del sistema: " +Math.floor(pot)+ " W/h"
  pot = Math.floor(pot*100/maxpot)
  document.getElementById('consumo').style = `width:${pot}%;`
  document.getElementById('consumo').innerText = pot +"%"
  var colorProgress = ""
  if (pot <= 25) {
    colorProgress = 'progress-bar progress-bar-striped progress-bar-animated'
  }else if (pot <= 50) {
    colorProgress = 'progress-bar progress-bar-striped progress-bar-animated bg-success'
  }else if (pot <= 75) {
    colorProgress = 'progress-bar progress-bar-striped progress-bar-animated bg-warning'
  }else {
    colorProgress = 'progress-bar progress-bar-striped progress-bar-animated bg-danger'
  }
  document.getElementById('consumo').className = colorProgress
}

function run(data){
  consumo_kw()
  data +='\n'
  console.log(data)
  enviar(data)
}

//*****	BEGIN WEBSOCKET ******//
// Connect2medialab
var mysocket;
var cardIP = "192.168.4.1";

function connect2medialab(){
    mysocket = new WebSocket(`ws://${cardIP}:3335`);
    waitForSocketConnection(mysocket, webSocketHandler("\r\n"));
}

// DisconnectMedialab
function disconnectWebsocket(){
    mysocket.close();
}

// Connection WebSocket
function waitForSocketConnection(socket, callback) {
  setTimeout(
    function () {
      if (socket.readyState == 1) {
        console.log("Connection is made")
    alert("Conexión establecida.");
        if(callback != null){
          // runCode()
          callback()
        }
        return;
      } else {
        console.log("Connecting timeOut.")
    alert("Se excedió el tiempo de espera.");
      }
  }, 4000); // wait 4000 milisecond for the connection...
}

function webSocketHandler(onMessageMsg) {
  mysocket.onopen = function() {
    console.log('Connection opened.');
  alert("Abriendo conexión...");
    connectionState = 1;
    //mysocket.send(onMessageMsg);
  }

  mysocket.onclose = function(e) {
  console.log("WebSocket Closed.");
  alert("Conexión cerrada.");
    connectionState = 0;
  }

  mysocket.onmessage = function(e) {
  console.log(e.data)
  lineCounter = lineCounter + 1
  if(lineCounter < lineas.length){
    mysocket.send(lineas[lineCounter]+"\r")
    console.log(lineas[lineCounter] + "\r\n")
    if (lineCounter == (lineas.length-1)){
      // alert("Programa enviado a Media_Lab y ejecutandose");
    }
  } else if(e.data.search("MicroPython")>-1){
    lineCounter = 0;
    lineas = [];
    console.log(e.data)
    alert("Reset en tarjeta Media_Lab")
  }
    else if(lineCounter == lineas.length){
    lineCounter = 0;
    lineas = [];
    // alert("Ejecución en Media_Lab terminada");
  }
  }
}

// Start Send Code
var lineCounter = 0;
var lineas = [];

function enviar(code){
  lineCounter = 0;
  lineas = [];
  lineas = code.split('\n');
  //console.log(lineas);
  mysocket.send(lineas[lineCounter]+"\r");
  console.log(lineas[lineCounter] + "\r\n")
}
// End send code
//*****	END WEBSOCKET ******//

